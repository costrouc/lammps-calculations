let
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/costrouc/nixpkgs/archive/277e3d723f01e690db49eb4feaaf8900610cb0e1.tar.gz";
    sha256 = "020i81rfhzzzc0fx132p1rxk2cazbpdpk2jz9viwa3hxjv07hksd";
  };
  pkgs = import nixpkgs { config = { }; };
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    python36Packages.dftfit
    python36Packages.lammps-cython
    python36Packages.pymatgen
    python36Packages.mpi4py
    python36Packages.gsd
    python36Packages.pyyaml
    openmpi
  ];

  shellHook = ''
    export NIX_PATH="nixpkgs=${nixpkgs}:."
  '';

}
