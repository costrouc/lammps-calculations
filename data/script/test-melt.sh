#!/usr/bin/env bash

NUM_CORES=1
DATE_NAME=$(date +"%Y-%m-%d-%H:%M")
RESULT_DIR=../results/auto
CONFIGURATION_DIR=../configuration/auto
mkdir -p $RESULT_DIR $CONFIGURATION_DIR
MELT_FILENAME=$CONFIGURATION_DIR/melt-$DATE_NAME.yaml

cat << EOF > $MELT_FILENAME
log_level: INFO

analysis_interval: 10          # timesteps
initial_temperature: 10    # K
melting_point_guess: 2000  # K

lammps_thermo: 1000        # timesteps
temp_damp: 0.5             # ps ( 500 fs)
press_damp: 1              # ps (1000 fs)

supercell: [4, 4, 8]

snapshots_filename: '$RESULT_DIR/melt-$DATE_NAME.gsd'
structure_filename: '../structure/mgo.cif'
potential_filename: '../potential/mgo-charge-buck.yaml'

steps:
  a: 0.1 # ps
  b: 0.1
  c: 0.1
  d: 0.1
EOF

mpirun -np $NUM_CORES ../../simulations/melt.py $MELT_FILENAME
