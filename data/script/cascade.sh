#!/usr/bin/env bash

NUM_CORES=4
DATE_NAME=$(date +"%Y-%m-%d-%H:%M")
RESULT_DIR=../results/auto
CONFIGURATION_DIR=../configuration/auto
mkdir -p $RESULT_DIR $CONFIGURATION_DIR
CASCADE_FILENAME=$CONFIGURATION_DIR/cascade-$DATE_NAME.yaml

cat << EOF > $CASCADE_FILENAME
log_level: INFO

initial_temperature: 10     # K
simulation_temperature: 273 # K  (Room Temperature)
temp_damp: 0.1              # ps ( 100 fs)
press_damp: 1               # ps (1000 fs)
supercell: [10, 10, 10]

analysis_interval: 1000
timestep:
  max_displacement: 0.075 # A/timestep
  max_timestep: 0.001    # ps

snapshots_filename: '../results/cascade-$DATE_NAME.gsd'
point_defect_filename: '../results/cascade-pd-$DATE_NAME.gsd'
structure_filename: '../structure/mgo.cif'
potential_filename: '../potential/mgo-charge-buck.yaml'

ion:
  element: Mg
  energy: 500 # eV

# Wigner seitz setup
wigner_seitz:
  boundary: 4.0 # A

steps:
  a: 300   # ps
  b: 5.0   # ps
EOF

mpirun -np $NUM_CORES ../../simulations/cascade.py $CASCADE_FILENAME
