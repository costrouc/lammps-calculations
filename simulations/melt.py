#!/usr/bin/env python

import sys
import random
from functools import partial

from utils import LammpsSimulation, read_cif_structure
import yaml

from dftfit.potential import Potential

c = yaml.load(open(sys.argv[1]))


### Simulation bellow
structure = read_cif_structure(c['structure_filename'], c['supercell'])
structure = structure.get_sorted_structure(key=lambda site: site.z)

solid_group_max_index = None
solid_group_max_z = structure.lattice.c / 2
for i, site in enumerate(structure):
    if site.z > (solid_group_max_z - 1e-1):
        solid_group_max_index = i
        break

potential = Potential.from_file(c['potential_filename'])
random_seed = random.randint(0, 10000000)

lmp_simulation = LammpsSimulation(structure, potential, log_level=c['log_level'])

write_snapshot = partial(lmp_simulation.write_snapshot,
                         filename=c['snapshots_filename'])
do_run = partial(lmp_simulation.run,
                 max_timestep_displacement=0.075, max_timestep=0.001,
                 analysis_interval=c['analysis_interval'],
                 analysis=[write_snapshot])


script = [
    # Initialize
    'group solid/group id <= %d' % solid_group_max_index,
    'group liquid/group subtract all solid/group',
    'thermo_style custom step vol temp press ke pe lx ly lz xy xz yz',
    'thermo %d' % c['lammps_thermo'],
    write_snapshot,

    # Relax
    'fix 1 all box/relax iso 0.0 vmax 0.001',
    'minimize 0 1.0e-8 100 1000',
    write_snapshot,
    'unfix 1',

    # Step A
    'velocity all create %f %d' % (c['initial_temperature'], random_seed),
    'velocity all zero linear',
    'fix 1 all npt temp %f %f %f iso 0.0 0.0 %f' % (
        c['initial_temperature'], c['melting_point_guess'],
        c['temp_damp'], c['press_damp']),
    partial(do_run, total_run_time=c['steps']['a']),
    'unfix 1',
    write_snapshot,

    # Step B
    'velocity liquid/group create %f %d' % (c['melting_point_guess'] * 2, random_seed),
    'velocity all zero linear',
    'fix 1 liquid/group nvt temp %f %f %f' % (c['melting_point_guess'] * 2, c['melting_point_guess'] * 2, random_seed),
    partial(do_run, total_run_time=c['steps']['b']),
    'unfix 1',
    write_snapshot,

    # Step C
    'velocity liquid/group create %f %d' % (c['melting_point_guess'], random_seed),
    'velocity all zero linear',
    'fix 1 liquid/group npt temp %f %f %f z 0.0 0.0 %f dilate liquid/group' % (c['melting_point_guess'], c['melting_point_guess'], c['temp_damp'], c['press_damp']),
    partial(do_run, total_run_time=c['steps']['c']),
    'unfix 1',
    write_snapshot,

    # Step D
    'fix 1 all nph z 0.0 0.0 %f' % c['press_damp'],
    partial(do_run, total_run_time=c['steps']['d']),
    write_snapshot
]

lmp_simulation.execute_script(script)
