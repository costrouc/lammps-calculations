import logging
import math

import numpy as np
from mpi4py import MPI
from pymatgen.io.cif import CifParser
import pymatgen as pmg
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from scipy.spatial import cKDTree
from gsd import hoomd

from dftfit.io.lammps_cython import write_potential, write_potential_files

import lammps


def read_cif_structure(filename, supercell):
    structure = CifParser(filename).get_structures()[0]
    lattice = pmg.Lattice.from_parameters(*structure.lattice.abc, *structure.lattice.angles)
    structure = pmg.Structure(lattice, structure.species, structure.frac_coords, coords_are_cartesian=False)
    spga = SpacegroupAnalyzer(structure)
    conventional_structure = spga.get_conventional_standard_structure()
    return conventional_structure * supercell


def read_gsd_structure(filename, index):
    with hoomd.open(filename, 'rb') as f:
        snapshot = f[index]

        lx, ly, lz, *tilts = snapshot.configuration.box
        bounds = [(0, lx), (0, ly), (0, lz)]
        lengths, angles_r, origin = lammps.core.lammps_box_to_lattice_const(bounds, tilts)
        angles = [math.degrees(a) for a in angles_r]
        lattice = pmg.Lattice.from_parameters(*lengths, *angles)

        species = snapshot.particles.types[snapshot.particles.typeid - 1]
        cart_coords = snapshot.particles.positions
        structure = pmg.Structure(
            lattice, species, cart_coords, coords_are_cartesian=True)
        if snapshot.particles.velocities:
            structure.add_site_property('velocities', snapshot.particles.velocities)


class LammpsSimulation:
    def __init__(self, structure, potential, log_level='WARNING'):
        # logging
        self._init_logging(log_level)
        self.logger = logging.getLogger(__name__)

        # MPI
        comm = MPI.COMM_WORLD
        self.SIZE = comm.Get_size()
        self.RANK = comm.Get_rank()

        # GSD snapshots
        self.GSD_FILE_HANDLES = {}

        # Initialize lammps calculation
        self.lammps, self.elements = self.initialize_lammps_calculation(
            structure, potential)

    def _init_logging(self, level='WARNING'):
        LOG_LEVELS = {'DEBUG', 'INFO', 'WARNING', 'CRITICAL', 'ERROR'}

        handler = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        logging.basicConfig(handlers=[handler], level=level.upper())

    def initialize_lammps_calculation(self, structure, potential):
        lmp = lammps.Lammps(units='metal', style='full', args=[
            '-log', 'none',
            '-screen', 'none'
        ])
        elements, rotation_matrix = lmp.system.add_pymatgen_structure(structure)

        if self.RANK == 0:
            lammps_files = write_potential_files(potential, elements)
            for filename, content in lammps_files:
                with open(filename, 'w') as f:
                    f.write(content)

        commands = write_potential(potential, elements)
        for command in (commands):
            lmp.command(command)

        return lmp, elements

    def execute_script(self, script):
        for command in script:
            if isinstance(command, str):
                self.lammps.command(command)
            elif callable(command):
                command(lmp=self.lammps, elements=self.elements)

    # helper methods
    def run(self, lmp, elements,
            total_run_time,
            max_timestep_displacement=0.1, max_timestep=0.001,
            analysis_interval=100, analysis=None):
        """Run simulation for set amount of time

        Parameters
        ==========
          - total_run_time: total time to run in lammps units [picoseconds]
          - max_timestep_displacement: max displacement [Angstroms]
          - max_timestep: max timestep to take [picoseconds]
          - analysis_interval: run analysis every X timesteps
          - analysis: analysis to run at each interval
        """
        analysis = analysis or []

        start_time = lmp.time
        remaining_time = total_run_time - (lmp.time - start_time)
        while remaining_time >= 0:
            # Set variable timestep
            velocities = lmp.system.velocities
            max_velocity = np.max(np.linalg.norm(velocities, axis=1)) # A/ps
            lmp.dt = min(max_timestep_displacement / max_velocity, max_timestep)

            if remaining_time / lmp.dt > analysis_interval:
                lmp.run(analysis_interval)
                for a in analysis:
                    a(lmp=lmp, elements=elements)
            else:
                lmp.run(int(remaining_time / lmp.dt) + 1)

            remaining_time = total_run_time - (lmp.time - start_time)

    def write_snapshot(self, lmp, elements, filename):
        elements = [str(e.symbol) for e in elements]
        snapshot = lmp.system.snapshot(elements, format='snapshot')
        temperature = lmp.thermo.computes['thermo_temp'].scalar

        if self.RANK == 0:
            snapshot.validate()
            if filename not in self.GSD_FILE_HANDLES:

                self.GSD_FILE_HANDLES[filename] = hoomd.open(filename, 'wb')
            self.logger.info('timestep: {:>8d} dt: {:>4.1g} temperature: {:>6.1f}'.format(lmp.time_step, lmp.dt, temperature))
            self.GSD_FILE_HANDLES[filename].append(snapshot)


def ev2Aps(Z, energy):
    # sqrt((2 * energy[eV] [J/eV]) / (amu [g/mole] [kg/g])) * [m/s] [A/ps]
    return math.sqrt((2 * energy * 1.6021766208e-19) / (Z / (6.02214085e23 * 1e3))) * 1e-2


def set_pka_atom(lmp, elements, energy, element):
    symbols = [e.symbol for e in elements]
    element_index = symbols.index(element)+1
    print(symbols, element, element_index)
    atom_index = np.where(lmp.system.types == element_index)[0][0]
    print(atom_index)
    velocities = lmp.system.velocities.copy()
    normalized_velocity = velocities[atom_index] / np.linalg.norm(velocities[atom_index])
    velocities[atom_index] = normalized_velocity * ev2Aps(pmg.Element(element).atomic_mass, energy)
    lmp.system.velocities = velocities


def variable_timestep(lmp, elements, max_timestep_displacement=0.1, max_timestep=0.001):
    velocities = lmp.system.velocities
    max_velocity = np.max(np.linalg.norm(velocities, axis=1)) # A/ps
    timestep = min(
        max_timestep_displacement / max_velocity,
        max_timestep)
    print(timestep, lmp.dt)
    lmp.dt = timestep


class WignerSeitzAnalysis:
    def __init__(self, filename):
        self.gsd = hoomd.open(filename, 'wb')

    def initialize(self, lmp, elements, boundary=5.0):
        self.reference_snapshot = lmp.system.snapshot(elements, format="snapshot")
        self.reference_snapshot.validate()

        lx, ly, lz, *tilts = self.reference_snapshot.configuration.box
        bounds = [(0, lx), (0, ly), (0, lz)]
        lengths, angles_r, origin = lammps.core.lammps_box_to_lattice_const(bounds, tilts)
        angles = [math.degrees(a) for a in angles_r]
        lattice = pmg.Lattice.from_parameters(*lengths, *angles)
        frac_boundary = boundary / min(lattice.abc)

        self.original_cart_coords = self.reference_snapshot.particles.position.copy()
        frac_coords = lattice.get_fractional_coords(self.reference_snapshot.particles.position)
        additional_indicies = []
        additional_coords = []

        # Corners
        x_min = frac_coords[:, 0] < frac_boundary
        x_max = frac_coords[:, 0] > 1 - frac_boundary
        y_min = frac_coords[:, 1] < frac_boundary
        y_max = frac_coords[:, 1] > 1 - frac_boundary
        z_min = frac_coords[:, 2] < frac_boundary
        z_max = frac_coords[:, 2] > 1 - frac_boundary

        conditions = [
            # Faces
            (x_min, ( 1,  0,  0)),
            (x_max, (-1,  0,  0)),
            (y_min, ( 0,  1,  0)),
            (y_max, ( 0, -1,  0)),
            (z_min, ( 0,  0,  1)),
            (z_max, ( 0,  0, -1)),
            # Edges
            (x_min & y_min, ( 1,  1,  0)),
            (x_min & y_max, ( 1, -1,  0)),
            (x_min & z_min, ( 1,  0,  1)),
            (x_min & z_max, ( 1,  0, -1)),
            (x_max & y_min, (-1,  1,  0)),
            (x_max & y_max, (-1, -1,  0)),
            (x_max & z_min, (-1,  0,  1)),
            (x_max & z_max, (-1,  0, -1)),
            (y_min & z_min, ( 0,  1,  1)),
            (y_min & z_max, ( 0,  1, -1)),
            (y_max & z_min, ( 0, -1,  1)),
            (y_max & z_max, ( 0, -1, -1)),
            # Corners
            (x_min & y_min & z_min, ( 1,  1,  1)),
            (x_min & y_min & z_max, ( 1,  1, -1)),
            (x_min & y_max & z_min, ( 1, -1,  1)),
            (x_min & y_max & z_max, ( 1, -1, -1)),
            (x_max & y_min & z_min, (-1,  1,  1)),
            (x_max & y_min & z_max, (-1,  1, -1)),
            (x_max & y_max & z_min, (-1, -1,  1)),
            (x_max & y_max & z_max, (-1, -1, -1)),
        ]

        additional_indicies = []
        additional_coords = []
        for condition, opperation in conditions:
            indicies = np.where(condition)[0]
            additional_indicies.append(indicies)
            additional_coords.append(frac_coords[indicies] + np.array(opperation))

        self.indicies = np.concatenate([np.arange(len(frac_coords))] + additional_indicies)
        cart_coords = lattice.get_cartesian_coords(np.concatenate([frac_coords] + additional_coords))
        print('num kdtree points', len(cart_coords))
        self.kdtree = cKDTree(cart_coords)

        # Add initial snapshot
        symbols = [e.symbol for e in elements]
        self.gsd.append(lmp.system.snapshot(symbols, format='snapshot'))

    def analyze(self, lmp, elements):
        state_snapshot = lmp.system.snapshot(elements, format="snapshot")
        state_snapshot.validate()
        distances, indexes = self.kdtree.query(state_snapshot.particles.position)

        # Map index -> actual atom index
        nearest_indexes = self.indicies[indexes]
        condition = np.where(nearest_indexes != np.arange(state_snapshot.particles.N))[0]
        # print(condition, nearest_index[condition], self.original_cart_coords[nearest_index[condition]], state_snapshot.particles.position[nearest_index[condition]])
        print(len(condition), 'point defects')

        # snapshot = hoomd.Snapshot()
        # snapshot.particles.N = len(condition)
        # snapshot.particles.positions = state_snapshot.particles.position[condition]
        # snapshot.particles.types = ['I', 'V']
        # snapshot.particles.typeid = np.zeros(len(condition))

        # snapshot.configuration.dimensions = state_snapshot.configuration.dimensions
        # snapshot.configuration.step = state_snapshot.configuration.step
        # snapshot.configuration.box = state_snapshot.configuration.box

        # snapshot.validate()
        # self.gsd.append(snapshot)
