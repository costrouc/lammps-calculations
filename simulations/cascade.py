#!/usr/bin/env python

import sys
import random
from functools import partial

from utils import LammpsSimulation, read_cif_structure, variable_timestep, set_pka_atom, WignerSeitzAnalysis
import yaml

from dftfit.potential import Potential

c = yaml.load(open(sys.argv[1]))


### Simulation bellow
structure = read_cif_structure(c['structure_filename'], c['supercell'])
structure = structure.get_sorted_structure(key=lambda site: site.z)

solid_group_max_index = None
solid_group_max_z = structure.lattice.c / 2
for i, site in enumerate(structure):
    if site.z > (solid_group_max_z - 1e-1):
        solid_group_max_index = i
        break

potential = Potential.from_file(c['potential_filename'])
random_seed = random.randint(0, 10000000)

lmp_simulation = LammpsSimulation(structure, potential, log_level=c['log_level'])

write_snapshot = partial(lmp_simulation.write_snapshot,
                         filename=c['snapshots_filename'])

wigner_sietz = WignerSeitzAnalysis(c['point_defect_filename'])

do_run = partial(lmp_simulation.run,
                 max_timestep_displacement=c['timestep']['max_displacement'],
                 max_timestep=c['timestep']['max_timestep'],
                 analysis_interval=c['analysis_interval'],
                 analysis=[
                     write_snapshot,
                 ])

do_cascade_run = partial(lmp_simulation.run,
                         max_timestep_displacement=c['timestep']['max_displacement'],
                         max_timestep=c['timestep']['max_timestep'],
                         analysis_interval=c['analysis_interval'],
                         analysis=[write_snapshot, wigner_sietz.analyze])

script = [
    # Initialize
    write_snapshot,

    # Relax
    'fix 1 all box/relax iso 0.0 vmax 0.001',
    'minimize 0 1.0e-8 100 1000',
    write_snapshot,
    'unfix 1',

    # Step A
    partial(wigner_sietz.initialize, boundary=c['wigner_sietz']['boundary']),
    'velocity all create %f %d' % (c['initial_temperature'], random_seed),
    'velocity all zero linear',
    'fix 1 all npt temp %f %f %f iso 0.0 0.0 %f' % (
        c['initial_temperature'], c['simulation_temperature'],
        c['temp_damp'], c['press_damp']),
    partial(do_run, total_run_time=c['steps']['a']),
    'unfix 1',
    write_snapshot,

    # Step B
    # wigner_sietz.initialize,
    partial(set_pka_atom, energy=c['ion']['energy'], element=c['ion']['element']),
    partial(variable_timestep, max_timestep_displacement=c['timestep']['max_displacement'], max_timestep=c['timestep']['max_timestep']),
    'velocity all zero linear',
    'fix 1 all nve',
    partial(do_cascade_run, total_run_time=c['steps']['b']),
    write_snapshot
]

lmp_simulation.execute_script(script)
